# LIS3781 - Advanced Database Management
## Alex Hernandez
### P2 Requirements:

#### README.md file should include the following items:
* Screenshot 
* Assessment Links

#### Assignment Screenshot and Link:

*Screenshot of P2*:

![P2](img/p2.png "P2") 

#### Bitbucket Repo Link:

*This assignment:*
[P2 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to P2 Remote Repo")
