# LIS3781 - Advanced Database Management
## Alex Hernandez
### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide Screenshots of Installations
    - Create Bitbucket Repo
    - Complete Bitbucket Tutorial
    - Provide Git Command Descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of SQL Code
    - Screenshot of Populated Tables
    - Assessment Links

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of SQL Code
    - Screenshot of Populated Tables
    - Assessment Links

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of ERD
    - Screenshot of SQL Statements
    - Assessment Links

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of ERD (MS SQL)
    - Assessment Links

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of ERD (MS SQL)
    - Assessment Links

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshot
    - Assessment Links