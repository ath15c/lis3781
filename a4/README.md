# LIS3781 - Advanced Database Management
## Alex Hernandez
### A4 Requirements:

#### README.md file should include the following items:
* Screenshot of ERD (MS SQL)
* Assessment Links

#### Assignment Screenshots and Links:

*Screenshot of A4 ERD*:

![A4 ERD](img/a4_erd.png "A4 ERD") 

#### Bitbucket Repo Links:

*This assignment:*
[A4 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to A4 Remote Repo")
