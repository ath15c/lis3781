# LIS3781 - Advanced Database Management
## Alex Hernandez
### A5 Requirements:

#### README.md file should include the following items:
* Screenshot of ERD (MS SQL)
* Assessment Links

#### Assignment Screenshots and Links:

*Screenshot of A5 ERD*:

![A5 ERD](img/a5_erd.png "A5 ERD") 

#### Bitbucket Repo Links:

*This assignment:*
[A5 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to A5 Remote Repo")
