# LIS3781 - Advanced Database Management
## Alex Hernandez
### P1 Requirements:

#### README.md file should include the following items:
* Screenshot of ERD
* Screenshot of SQL Statements
* Assessment Links

#### Assignment Screenshots and Links:

*Screenshot P1 ERD*:

![P1 ERD](img/p1_erd.png "P1 ERD") 

*Screenshot P1 SQL Statements*:

![P1 SQL Statements](img/ss1.png "P1 SQL Statements") 
![P1 SQL Statements](img/ss2.png "P1 SQL Statements") 

#### Bitbucket Repo Links:

*This assignment:*
[P1 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to P1 Remote Repo")
