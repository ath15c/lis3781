# LIS3781 - Advanced Database Management
## Alex Hernandez
### A2 Requirements:

#### README.md file should include the following items:
* Screenshot of SQL Code
* Screenshot of Populated Tables
* Assessment Links

#### Assignment Screenshots and Links:

*Screenshots A2 SQL Code*:

![A2 SQL 1](img/a2_sql_code_a.png "A2 SQL Code A") 
![A2 SQL 2](img/a2_sql_code_b.png "A2 SQL Code B") 

*Screenshot A2 Populated Tables*:

![A2 Populated Tables](img/a2_populated_tables_screenshot.png "A2 Populated Tables") 

#### Bitbucket Repo Links:

*This assignment:*
[A2 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to A2 Remote Repo")
