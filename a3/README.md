# LIS3781 - Advanced Database Management
## Alex Hernandez
### A3 Requirements:

#### README.md file should include the following items:
* Screenshot of SQL Code
* Screenshot of Populated Tables
* Assessment Links

#### Assignment Screenshots and Links:

*Screenshots A3 SQL Code*:

![A3 SQL 1](img/a3_sql_a.png "A3 SQL Code A") 
![A3 SQL 2](img/a3_sql_b.png "A3 SQL Code B") 

*Screenshot A3 Populated Tables*:

![A3 Populated Tables](img/a3_tables.png "A3 Populated Tables") 

#### Bitbucket Repo Links:

*This assignment:*
[A3 Bitbucket Repo](https://bitbucket.org/ath15c/lis3781/ "Link to A3 Remote Repo")
